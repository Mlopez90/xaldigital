package com.mlopez.xaldidigital;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mlopez.xaldidigital.main.view.MainViewImpl;

import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SplashActivity extends AppCompatActivity {
    private Context context;
    private static final int SPLASH_SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        initRealm();
        runTimeSplash();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("xaldigitalDB")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private void runTimeSplash() {
        TimerTask taskSplash = new TimerTask() {
            @Override
            public void run() {
                Intent main = new Intent(context, MainViewImpl.class);
                startActivity(main);
                finish();
            }
        };

        Timer timer = new Timer();
        timer.schedule(taskSplash, SPLASH_SCREEN_DELAY);
    }
}

