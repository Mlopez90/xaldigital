package com.mlopez.xaldidigital;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.history.view.ItemHistoryDetailAdapter;
import com.mlopez.xaldidigital.main.repository.DTO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Util {

    public static byte FROM_MAIN = 1;
    public static byte FROM_MODAL = 2;

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static class Dates {
        public static String convertToString(Date date) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return sdf.format(date);
        }
    }

    public static class Modal {
        private static AlertDialog dialog;

        public static void withListView(Context context, List<DTO.Country> data, String name, String date){
            Button btnClose;
            TextView tvName;
            TextView tvDate;
            RecyclerView itemList;
            RecyclerView.LayoutManager layoutManager;

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            final AlertDialog dialog = builder.create();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.modal_history_detail,null);

            tvName = view.findViewById(R.id.tvName);
            tvDate = view.findViewById(R.id.tvDate);
            itemList = view.findViewById(R.id.itemList);
            btnClose = view.findViewById(R.id.btnClose);

            layoutManager = new LinearLayoutManager(context);
            itemList.setLayoutManager(layoutManager);

            ItemHistoryDetailAdapter adapter = new ItemHistoryDetailAdapter(data, context);
            itemList.setAdapter(adapter);

            tvName.setText(name);
            tvDate.setText(date);

            btnClose.setOnClickListener(v -> {
                dialog.dismiss();
            });

            dialog.setView(view);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        public static void loading(Context context){
            if (context != null){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                AlertDialog dialog = builder.create();

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View modal = inflater.inflate(R.layout.modal_loading, null);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setView(modal);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCanceledOnTouchOutside(false);

                dialog.setOnKeyListener((dialogInterface, i, keyEvent) -> {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN){
                        if (i == KeyEvent.KEYCODE_BACK || i == KeyEvent.KEYCODE_HOME){
                            return false;
                        }
                    }
                    return true;
                });

                if (Modal.dialog == null || !Modal.dialog.isShowing()){
                    Modal.dialog = dialog;
                    dialog.show();
                }
            }else{
                System.out.println("------ Error context null - show modal dialog");
            }
        }

        public static void stopLoading(){
            if(Modal.dialog != null && Modal.dialog.isShowing()){
                System.out.println(Modal.dialog);
                Modal.dialog.hide();
                Modal.dialog.cancel();
                Modal.dialog = null;
            }
        }
    }
}
