package com.mlopez.xaldidigital.main.interactor;

import org.json.JSONObject;

public interface MainInteractor {
    void serviceName(String name);
    void error(String error);
    void successGetServiceName(JSONObject data);
}
