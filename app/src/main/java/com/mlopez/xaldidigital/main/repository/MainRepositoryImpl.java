package com.mlopez.xaldidigital.main.repository;

import android.content.Context;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mlopez.xaldidigital.main.interactor.MainInteractor;

public class MainRepositoryImpl implements MainRepository{
    private MainInteractor interactor;
    private Context context;

    public MainRepositoryImpl(MainInteractor interactor, Context context) {
        this.interactor = interactor;
        this.context = context;
    }

    @Override
    public void callServiceName(String name) {
        String url = "https://api.nationalize.io/?name=".concat(name);

        JsonObjectRequest request = new  JsonObjectRequest(Request.Method.GET, url, null, response -> {
            interactor.successGetServiceName(response);
        }, error -> {
            if (error instanceof NoConnectionError) {
                interactor.error("Favor de verificar su conexion a internet");
            } else if (error instanceof ServerError) {
                interactor.error("Ocurrio un error con el servidor");
            } else if (error instanceof TimeoutError) {
                interactor.error("Se termino el tiempo de espera");
            } else {
                interactor.error("Ocurrio un error, intente mas tarde");
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(request);
    }

}
