package com.mlopez.xaldidigital.main.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.R;
import com.mlopez.xaldidigital.Util;
import com.mlopez.xaldidigital.history.view.HistoryViewImpl;
import com.mlopez.xaldidigital.main.presenter.MainPresenter;
import com.mlopez.xaldidigital.main.presenter.MainPresenterImpl;
import com.mlopez.xaldidigital.main.repository.DTO;

import static com.mlopez.xaldidigital.Util.FROM_MAIN;

public class MainViewImpl extends AppCompatActivity implements MainView {
    private EditText etName;
    private Button btnConsultar;
    private TextView tvName;
    private MainPresenter mainPresenter;
    private ItemAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView itemList;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenterImpl(this, this);

        etName = findViewById(R.id.etName);
        tvName = findViewById(R.id.tvName);
        itemList = findViewById(R.id.itemList);

        btnConsultar = findViewById(R.id.btnConsultar);
        btnConsultar.setOnClickListener(consultaClick);
        etName.setOnEditorActionListener(etListener);

        layoutManager = new LinearLayoutManager(this);
        itemList.setLayoutManager(layoutManager);
    }

    View.OnClickListener consultaClick = view -> {
        Util.hideKeyboard(MainViewImpl.this);
        initCallService();
    };

    TextView.OnEditorActionListener etListener = ((textView, i, keyEvent) -> {
        if (i == EditorInfo.IME_ACTION_DONE) {
            initCallService();
        }
        return false;
    });

    private void initCallService() {
        tvName.setText("");
        adapter = new ItemAdapter(null, context, FROM_MAIN);
        itemList.setAdapter(adapter);
        mainPresenter.callServiceName(etName.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_history) {
            Intent history = new Intent(context, HistoryViewImpl.class);
            startActivity(history);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void successGetService(DTO.Name result) {
        etName.setText("");
        tvName.setText(result.getName());

        adapter = new ItemAdapter(result.getCountry(), this, FROM_MAIN);
        itemList.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        this.btnConsultar.setEnabled(false);
        Util.Modal.loading(this);
    }

    @Override
    public void hideProgress() {
        this.btnConsultar.setEnabled(true);
        Util.Modal.stopLoading();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

}
