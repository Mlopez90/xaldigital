package com.mlopez.xaldidigital.main.view;

import com.mlopez.xaldidigital.main.repository.DTO;

public interface MainView {
    void successGetService(DTO.Name result);
    void showProgress();
    void hideProgress();
    void showError(String error);
}
