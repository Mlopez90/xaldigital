package com.mlopez.xaldidigital.main.interactor;

import android.content.Context;

import com.google.gson.Gson;
import com.mlopez.xaldidigital.Util;
import com.mlopez.xaldidigital.history.model.History;
import com.mlopez.xaldidigital.main.presenter.MainPresenter;
import com.mlopez.xaldidigital.main.repository.DTO;
import com.mlopez.xaldidigital.main.repository.MainRepository;
import com.mlopez.xaldidigital.main.repository.MainRepositoryImpl;

import org.json.JSONObject;
import java.util.Date;
import io.realm.Realm;

public class MainInteractorImpl implements MainInteractor{
    private MainPresenter presenter;
    private MainRepository repository;
    private Realm realm;

    public MainInteractorImpl(MainPresenter presenter, Context context) {
        this.presenter = presenter;
        this.repository = new MainRepositoryImpl(this, context);
    }
    @Override
    public void serviceName(String name) {
        if (name.isEmpty()) {
            error("Favor de ingresar un nombre");
        } else {
            this.repository.callServiceName(name);
        }
    }

    @Override
    public void error(String error) {
        presenter.error(error);
    }

    @Override
    public void successGetServiceName(JSONObject data) {
        try {
            Gson gson = new Gson();
            DTO.Name dtoName  = gson.fromJson(data.toString(), DTO.Name.class);

            if(dtoName.getCountry().isEmpty()) {
                error("No se encontraron coincidencias");
            } else {

                for(DTO.Country c : dtoName.getCountry()) {
                    c.setUrlImage("http://www.geognos.com/api/en/countries/flag/".concat(c.getCountry_id().toUpperCase()).concat(".png"));
                }

                realm = Realm.getDefaultInstance();
                realm.beginTransaction();

                Number maxId = realm.where(History.class).max("id");
                Long nextId = (maxId == null) ? 1 : maxId.longValue()+1;
                History history = gson.fromJson(data.toString(), History.class);
                history.setId(nextId);
                history.setCreatedAt(Util.Dates.convertToString(new Date()));
                realm.insert(history);
                realm.commitTransaction();
                realm.close();

                this.presenter.successGetService(dtoName);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            error(e.getMessage());
        }
    }
}
