package com.mlopez.xaldidigital.main.presenter;

import com.mlopez.xaldidigital.main.repository.DTO;

public interface MainPresenter {
    void callServiceName(String name);
    void successGetService(DTO.Name data);
    void error(String error);
}
