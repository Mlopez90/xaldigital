package com.mlopez.xaldidigital.main.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.R;
import com.mlopez.xaldidigital.main.repository.DTO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mlopez.xaldidigital.Util.FROM_MODAL;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private List<DTO.Country> dataSource;
    private Context context;
    private byte origin;

    public ItemAdapter(List<DTO.Country> dataSource, Context context, byte origin) {
        this.dataSource = dataSource == null ? new ArrayList<>() : dataSource;
        this.context = context;
        this.origin = origin;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgVCountry;
        private TextView tvCountry;
        private TextView tvProbability;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgVCountry = itemView.findViewById(R.id.imgVCountry);
            tvCountry = itemView.findViewById(R.id.tvCountry);
            tvProbability = itemView.findViewById(R.id.tvProbability);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(origin == FROM_MODAL) {
            ViewGroup.LayoutParams layoutParams = holder.imgVCountry.getLayoutParams();
            layoutParams.width = 40;
            layoutParams.height = 40;
            holder.imgVCountry.setLayoutParams(layoutParams);
        }
        System.out.println(dataSource.get(position).getProbability());
        holder.tvCountry.setText(dataSource.get(position).getCountry_id());
        holder.tvProbability.setText(String.valueOf(dataSource.get(position).getProbability()));
        Picasso.get().load(dataSource.get(position).getUrlImage()).into(holder.imgVCountry);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }


}
