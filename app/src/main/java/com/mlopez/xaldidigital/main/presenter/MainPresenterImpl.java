package com.mlopez.xaldidigital.main.presenter;

import android.content.Context;

import com.mlopez.xaldidigital.main.interactor.MainInteractor;
import com.mlopez.xaldidigital.main.interactor.MainInteractorImpl;
import com.mlopez.xaldidigital.main.repository.DTO;
import com.mlopez.xaldidigital.main.view.MainView;

public class MainPresenterImpl implements MainPresenter{

    private MainView mainView;
    private MainInteractor interactor;

    public MainPresenterImpl(MainView mainView, Context context) {
        this.mainView = mainView;
        this.interactor = new MainInteractorImpl(this, context);
    }

    @Override
    public void callServiceName(String name) {
        this.mainView.showProgress();
        this.interactor.serviceName(name);
    }

    @Override
    public void successGetService(DTO.Name data) {
        this.mainView.hideProgress();
        this.mainView.successGetService(data);
    }

    @Override
    public void error(String error) {
        this.mainView.hideProgress();
        this.mainView.showError(error);
    }
}
