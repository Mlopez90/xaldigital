package com.mlopez.xaldidigital.main.repository;

import java.util.List;

public class DTO {
    public static class Country {
        private String country_id;
        private Double probability;
        private String urlImage;

        public Country() {}

        public Country(String country_id, Double probability, String urlImage) {
            this.country_id = country_id;
            this.probability = probability;
            this.urlImage = urlImage;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public Double getProbability() {
            return probability;
        }

        public void setProbability(Double probability) {
            this.probability = probability;
        }

        public String getUrlImage() {
            return urlImage;
        }

        public void setUrlImage(String urlImage) {
            this.urlImage = urlImage;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "country_id='" + country_id + '\'' +
                    ", probability=" + probability +
                    ", urlImage='" + urlImage + '\'' +
                    '}';
        }
    }

    public static class Name {
        private String name;
        private List<Country> country;
        private String date;

        public Name(){}

        public Name(String name, List<Country> country) {
            this.name = name;
            this.country = country;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Country> getCountry() {
            return country;
        }

        public void setCountry(List<Country> country) {
            this.country = country;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "name='" + name + '\'' +
                    ", country=" + country +
                    ", date='" + date + '\'' +
                    '}';
        }
    }
}
