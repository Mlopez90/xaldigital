package com.mlopez.xaldidigital.history.interactor;

import java.util.Date;

public interface HistoryInteractor {
    void queryHistoryData(Date date);
}
