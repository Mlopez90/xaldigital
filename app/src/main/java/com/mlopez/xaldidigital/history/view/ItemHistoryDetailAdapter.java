package com.mlopez.xaldidigital.history.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.R;
import com.mlopez.xaldidigital.main.repository.DTO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemHistoryDetailAdapter extends RecyclerView.Adapter<ItemHistoryDetailAdapter.ViewHolder>{

    private List<DTO.Country> dataSource;
    private Context context;
    private byte origin;

    public ItemHistoryDetailAdapter(List<DTO.Country> dataSource, Context context) {
        this.dataSource = dataSource == null ? new ArrayList<>() : dataSource;
        this.context = context;
        this.origin = origin;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgVCountry;
        private TextView tvCountry;
        private TextView tvProbability;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgVCountry = itemView.findViewById(R.id.imgVCountry);
            tvCountry = itemView.findViewById(R.id.tvCountry);
            tvProbability = itemView.findViewById(R.id.tvProbability);
        }
    }


    @NonNull
    @Override
    public ItemHistoryDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail, parent, false);

        return new ItemHistoryDetailAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHistoryDetailAdapter.ViewHolder holder, int position) {
        holder.tvCountry.setText(dataSource.get(position).getCountry_id());
        holder.tvProbability.setText(String.valueOf(dataSource.get(position).getProbability()));
        Picasso.get().load(dataSource.get(position).getUrlImage()).into(holder.imgVCountry);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }


}
