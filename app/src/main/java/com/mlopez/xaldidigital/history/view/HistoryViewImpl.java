package com.mlopez.xaldidigital.history.view;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.R;
import com.mlopez.xaldidigital.Util;
import com.mlopez.xaldidigital.history.presenter.HistoryPresenter;
import com.mlopez.xaldidigital.history.presenter.HistoryPresenterImpl;
import com.mlopez.xaldidigital.main.repository.DTO;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class HistoryViewImpl extends AppCompatActivity implements HistoryView{
    private View fragmentEmpty;
    private HistoryPresenter presenter;
    private RecyclerView historyList;
    private LinearLayout containerList;
    private Date currentDateSelected;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getSupportActionBar().setTitle(getResources().getString(R.string.history));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.presenter = new HistoryPresenterImpl(this);
        containerList = findViewById(R.id.containerList);
        fragmentEmpty = findViewById(R.id.fragmentEmpty);
        historyList = findViewById(R.id.historyList);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        historyList.setLayoutManager(manager);

        currentDateSelected = new Date();
        this.presenter.getHistoryData(currentDateSelected);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.history_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();

        } else if(item.getItemId() == R.id.action_filter) {
            final Calendar c = Calendar.getInstance();
            c.setTime(currentDateSelected);

            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                    Date d = new GregorianCalendar(year, month, day).getTime();
                    currentDateSelected = d;
                    presenter.getHistoryData(d);
                }
            ,c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resultHistoryData(List<DTO.Name> data) {
        if (data.isEmpty()) {
            containerList.setVisibility(View.GONE);
            fragmentEmpty.setVisibility(View.VISIBLE);
        } else {
            containerList.setVisibility(View.VISIBLE);
            fragmentEmpty.setVisibility(View.GONE);
            ItemHistoryAdapter adapter = new ItemHistoryAdapter(data, this);
            historyList.setAdapter(adapter);
        }
    }

    @Override
    public void showProgress() {
        Util.Modal.loading(this);
    }

    @Override
    public void hideProgress() {
        Util.Modal.stopLoading();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
