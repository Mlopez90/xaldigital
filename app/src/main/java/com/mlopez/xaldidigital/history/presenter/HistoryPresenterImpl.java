package com.mlopez.xaldidigital.history.presenter;

import com.mlopez.xaldidigital.history.interactor.HistoryInteractor;
import com.mlopez.xaldidigital.history.interactor.HistoryInteractorImpl;
import com.mlopez.xaldidigital.history.view.HistoryView;
import com.mlopez.xaldidigital.main.repository.DTO;

import java.util.Date;
import java.util.List;

public class HistoryPresenterImpl implements HistoryPresenter{
    private HistoryView view;
    private HistoryInteractor interactor;

    public HistoryPresenterImpl(HistoryView view) {
        this.view = view;
        this.interactor = new HistoryInteractorImpl(this);
    }

    @Override
    public void getHistoryData(Date date) {
        this.view.showProgress();
        this.interactor.queryHistoryData(date);
    }

    @Override
    public void resultHistoryData(List<DTO.Name> result) {
        this.view.hideProgress();
        this.view.resultHistoryData(result);
    }

    @Override
    public void error(String error) {
        this.view.hideProgress();
        this.view.showError(error);
    }
}
