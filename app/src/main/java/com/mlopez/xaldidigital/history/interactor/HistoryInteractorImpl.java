package com.mlopez.xaldidigital.history.interactor;

import com.mlopez.xaldidigital.Util;
import com.mlopez.xaldidigital.history.model.CountryProbability;
import com.mlopez.xaldidigital.history.model.History;
import com.mlopez.xaldidigital.history.presenter.HistoryPresenter;
import com.mlopez.xaldidigital.main.repository.DTO;
import com.mlopez.xaldidigital.main.repository.DTO.Country;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class HistoryInteractorImpl implements HistoryInteractor{
    private HistoryPresenter presenter;


    public HistoryInteractorImpl(HistoryPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void queryHistoryData(Date date) {

        List<DTO.Name> result = new ArrayList<>();

        try {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();

            RealmResults<History> historyQ = realm.where(History.class).contains("createdAt", Util.Dates.convertToString(date).substring(0,10)).findAll();
            for (History h: historyQ) {
                List<Country> countries = new ArrayList<>();

                for (CountryProbability c : h.getCountry()){
                    Country  country = new Country(c.getCountry_id(), c.getProbability(), "http://www.geognos.com/api/en/countries/flag/".concat(c.getCountry_id().toUpperCase()).concat(".png"));
                    countries.add(country);
                }
                DTO.Name n = new DTO.Name(h.getName(), countries);
                n.setDate(h.getCreatedAt());
                result.add(n);
            }

            realm.close();

            this.presenter.resultHistoryData(result);

        }catch (Exception e) {
            e.printStackTrace();
            this.presenter.error(e.getLocalizedMessage());
        }
    }
}
