package com.mlopez.xaldidigital.history.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mlopez.xaldidigital.R;
import com.mlopez.xaldidigital.Util;
import com.mlopez.xaldidigital.main.repository.DTO;

import java.util.ArrayList;
import java.util.List;

public class ItemHistoryAdapter extends RecyclerView.Adapter<ItemHistoryAdapter.ViewHolder>{
    private List<DTO.Name> dataSource;
    private Context context;


    public ItemHistoryAdapter(List<DTO.Name> dataSource, Context context) {
        this.dataSource =  dataSource == null ? new ArrayList<>() : dataSource;;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvDate;
        private ImageButton imgBtnDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvDate = itemView.findViewById(R.id.tvDate);
            imgBtnDetail = itemView.findViewById(R.id.imgBtnDetail);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(dataSource.get(position).getName());
        holder.tvDate.setText(dataSource.get(position).getDate());
        holder.imgBtnDetail.setOnClickListener(view -> {
            Util.Modal.withListView(context, dataSource.get(position).getCountry(), dataSource.get(position).getName(),dataSource.get(position).getDate());
        });
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

}
