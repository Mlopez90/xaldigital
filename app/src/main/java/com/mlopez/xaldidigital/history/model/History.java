package com.mlopez.xaldidigital.history.model;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class History extends RealmObject implements Serializable {

    @PrimaryKey
    private Long id;

    private String name;
    private RealmList<CountryProbability> country;
    private String createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<CountryProbability> getCountry() {
        return country;
    }

    public void setCountry(RealmList<CountryProbability> country) {
        this.country = country;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
