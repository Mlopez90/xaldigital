package com.mlopez.xaldidigital.history.presenter;

import com.mlopez.xaldidigital.main.repository.DTO;

import java.util.Date;
import java.util.List;

public interface HistoryPresenter {
    void getHistoryData(Date date);
    void resultHistoryData(List<DTO.Name> result);
    void error(String error);
}
