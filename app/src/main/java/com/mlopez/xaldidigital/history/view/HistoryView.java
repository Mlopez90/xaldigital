package com.mlopez.xaldidigital.history.view;

import com.mlopez.xaldidigital.main.repository.DTO;

import java.util.List;

public interface HistoryView {
    void resultHistoryData(List<DTO.Name> data);
    void showProgress();
    void hideProgress();
    void showError(String error);
}
