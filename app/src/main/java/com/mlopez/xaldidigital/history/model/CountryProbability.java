package com.mlopez.xaldidigital.history.model;

import java.io.Serializable;

import io.realm.RealmObject;


public class CountryProbability extends RealmObject implements Serializable {
    private String country_id;
    private Double probability;

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public Double getProbability() {
        return probability;
    }

    public void setProbability(Double probability) {
        this.probability = probability;
    }
}
